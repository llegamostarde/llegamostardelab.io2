---
title: "#01 Katsuhiro Otomo"
date: 2018-12-28
author: Llegamos tarde
category: [Llegamos Tarde]
featimg: 2018/miniatura1.png
podcast:
  audio: https://archive.org/download/LlegamosTarde1KatsuhiroOtomo/Llegamos%20Tarde%20%231%20-%20Katsuhiro%20Otomo
  video: https://www.youtube.com/watch?v=w65MXavhkoA
tags: [audio, podcast, Llegamos Tarde]
comments: true
---
![](https://llegamostarde.gitlab.io/media/compressed/2018/miniatura1.png)  
<audio controls>
  <source src="https://archive.org/download/LlegamosTarde1KatsuhiroOtomo/Llegamos%20Tarde%20%231%20-%20Katsuhiro%20Otomo.mp3" type="audio/ogg">
  <source src="https://archive.org/download/LlegamosTarde1KatsuhiroOtomo/Llegamos%20Tarde%20%231%20-%20Katsuhiro%20Otomo.mp3" type="audio/mpeg">
</audio>





Recuerda que puedes **contactar** con nosotros:

Twitter: <https://twitter.com/llegamostarde_>  
Mastodon: </>  
Correo: <llegamostardeyt@gmail.com>  
Blog: <https://llegamostarde.github.io/>  
Telegram: <>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/llegamostarde>  
