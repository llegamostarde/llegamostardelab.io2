---
layout: page-classic-sidebar-right
title: Contacto
---
Puedes <strong>contactarnos</strong> en las siguientes páginas:
<ul>
 	<li>Twitter: <a href="https://twitter.com/llegamostarde_">@llegamostarde_</a></li>
 	<li>Correo: <a href="mailto:llegamostardepodcast@gmail.com">llegamostardepodcast@gmail.com</a></li>
 	<li>Web: <a href="https://llegamostarde.gitlab.io/">https://llegamostarde.gitlab.io</a></li>
 	<li>Blog: <a href="https://llegamostarde.gitlab.io">https://llegamostarde.gitlab.io</a></li>

</ul>
<br>

Principalmente estamos en <a href="https://www.youtube.com/channel/UCXsdwLDyCKk0cTkYhIblnQQ">Youtube</a>.


<br>

El feed del programa oficial  
<a href="https://feedpress.me/llegamostarde">https://feedpress.me/llegamostarde</a>  

